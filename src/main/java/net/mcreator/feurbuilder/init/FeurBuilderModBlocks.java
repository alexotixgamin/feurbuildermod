
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.feurbuilder.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.RegisterColorHandlersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import net.mcreator.feurbuilder.block.YellowStairsBlock;
import net.mcreator.feurbuilder.block.YellowSlabBlock;
import net.mcreator.feurbuilder.block.YellowSealanternBlock;
import net.mcreator.feurbuilder.block.YellowPlanksBlock;
import net.mcreator.feurbuilder.block.YellowLogBlock;
import net.mcreator.feurbuilder.block.YellowLeavesWallBlock;
import net.mcreator.feurbuilder.block.YellowLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.YellowLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.YellowLeavesBlock;
import net.mcreator.feurbuilder.block.YellowFenceGateBlock;
import net.mcreator.feurbuilder.block.YellowFenceBlock;
import net.mcreator.feurbuilder.block.YellowBricksCrackedBlock;
import net.mcreator.feurbuilder.block.YellowBricksBlock;
import net.mcreator.feurbuilder.block.YellowBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.WarpedBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.SpruceLeavesWallBlock;
import net.mcreator.feurbuilder.block.SpruceBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.PurpleStairsBlock;
import net.mcreator.feurbuilder.block.PurpleSlabBlock;
import net.mcreator.feurbuilder.block.PurpleSealanternBlock;
import net.mcreator.feurbuilder.block.PurplePlanksBlock;
import net.mcreator.feurbuilder.block.PurpleLogBlock;
import net.mcreator.feurbuilder.block.PurpleLeavesWallBlock;
import net.mcreator.feurbuilder.block.PurpleLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.PurpleLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.PurpleLeavesBlock;
import net.mcreator.feurbuilder.block.PurpleFenceGateBlock;
import net.mcreator.feurbuilder.block.PurpleFenceBlock;
import net.mcreator.feurbuilder.block.PurpleBricksCrackedBlock;
import net.mcreator.feurbuilder.block.PurpleBricksBlock;
import net.mcreator.feurbuilder.block.PurpleBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.PinklogBlock;
import net.mcreator.feurbuilder.block.PinkStairsBlock;
import net.mcreator.feurbuilder.block.PinkSlabBlock;
import net.mcreator.feurbuilder.block.PinkSealanternBlock;
import net.mcreator.feurbuilder.block.PinkPlanksBlock;
import net.mcreator.feurbuilder.block.PinkLeavesWallBlock;
import net.mcreator.feurbuilder.block.PinkLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.PinkLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.PinkLeavesBlock;
import net.mcreator.feurbuilder.block.PinkFenceGateBlock;
import net.mcreator.feurbuilder.block.PinkFenceBlock;
import net.mcreator.feurbuilder.block.PinkBricksCrackedBlock;
import net.mcreator.feurbuilder.block.PinkBricksBlock;
import net.mcreator.feurbuilder.block.PinkBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.OrangeSealanternBlock;
import net.mcreator.feurbuilder.block.OrangeBricksCrackedBlock;
import net.mcreator.feurbuilder.block.OrangeBricksBlock;
import net.mcreator.feurbuilder.block.OakLeavesWallBlock;
import net.mcreator.feurbuilder.block.OakBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.MangroveGlassPanelBlock;
import net.mcreator.feurbuilder.block.MangroveGlassBlock;
import net.mcreator.feurbuilder.block.MangroveBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.JungleBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.GreyStairsBlock;
import net.mcreator.feurbuilder.block.GreySlabBlock;
import net.mcreator.feurbuilder.block.GreySealanternBlock;
import net.mcreator.feurbuilder.block.GreyPlanksBlock;
import net.mcreator.feurbuilder.block.GreyLogBlock;
import net.mcreator.feurbuilder.block.GreyLeavesWallBlock;
import net.mcreator.feurbuilder.block.GreyLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.GreyLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.GreyLeavesBlock;
import net.mcreator.feurbuilder.block.GreyFenceGateBlock;
import net.mcreator.feurbuilder.block.GreyFenceBlock;
import net.mcreator.feurbuilder.block.GreyBricksCrackedBlock;
import net.mcreator.feurbuilder.block.GreyBricksBlock;
import net.mcreator.feurbuilder.block.GreyBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.GreenlogBlock;
import net.mcreator.feurbuilder.block.GreenStairsBlock;
import net.mcreator.feurbuilder.block.GreenSlabBlock;
import net.mcreator.feurbuilder.block.GreenSealanternBlock;
import net.mcreator.feurbuilder.block.GreenPlanksBlock;
import net.mcreator.feurbuilder.block.GreenLeavesWallBlock;
import net.mcreator.feurbuilder.block.GreenLeavesBlock;
import net.mcreator.feurbuilder.block.GreenFenceGateBlock;
import net.mcreator.feurbuilder.block.GreenFenceBlock;
import net.mcreator.feurbuilder.block.GreenBricksCrackedBlock;
import net.mcreator.feurbuilder.block.GreenBricksBlock;
import net.mcreator.feurbuilder.block.GreenBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.DarkOakBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.CyanlogBlock;
import net.mcreator.feurbuilder.block.CyanStairsBlock;
import net.mcreator.feurbuilder.block.CyanSlabBlock;
import net.mcreator.feurbuilder.block.CyanPlanksBlock;
import net.mcreator.feurbuilder.block.CyanLeavesWallBlock;
import net.mcreator.feurbuilder.block.CyanLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.CyanLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.CyanLeavesBlock;
import net.mcreator.feurbuilder.block.CyanFenceGateBlock;
import net.mcreator.feurbuilder.block.CyanFenceBlock;
import net.mcreator.feurbuilder.block.CyanBricksCrackedBlock;
import net.mcreator.feurbuilder.block.CyanBricksBlock;
import net.mcreator.feurbuilder.block.CyanBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.CrimsonBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.CalciteSmoothBlock;
import net.mcreator.feurbuilder.block.CalciteDeepBlock;
import net.mcreator.feurbuilder.block.BluelogBlock;
import net.mcreator.feurbuilder.block.BlueleavesfloweringBlock;
import net.mcreator.feurbuilder.block.BlueStairsBlock;
import net.mcreator.feurbuilder.block.BlueSlabBlock;
import net.mcreator.feurbuilder.block.BlueSealanternBlock;
import net.mcreator.feurbuilder.block.BluePlanksBlock;
import net.mcreator.feurbuilder.block.BlueLeavesWallBlock;
import net.mcreator.feurbuilder.block.BlueLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.BlueLeavesBlock;
import net.mcreator.feurbuilder.block.BlueFenceGateBlock;
import net.mcreator.feurbuilder.block.BlueFenceBlock;
import net.mcreator.feurbuilder.block.BlueBricksCrackedBlock;
import net.mcreator.feurbuilder.block.BlueBricksBlock;
import net.mcreator.feurbuilder.block.BlueBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.BlacklogBlock;
import net.mcreator.feurbuilder.block.BlackStairsBlock;
import net.mcreator.feurbuilder.block.BlackSlabBlock;
import net.mcreator.feurbuilder.block.BlackPlanksBlock;
import net.mcreator.feurbuilder.block.BlackLeavesWallBlock;
import net.mcreator.feurbuilder.block.BlackLeavesFloweringWallBlock;
import net.mcreator.feurbuilder.block.BlackLeavesFloweringBlock;
import net.mcreator.feurbuilder.block.BlackLeavesBlock;
import net.mcreator.feurbuilder.block.BlackFenceGateBlock;
import net.mcreator.feurbuilder.block.BlackFenceBlock;
import net.mcreator.feurbuilder.block.BlackBricksBlock;
import net.mcreator.feurbuilder.block.BlackBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.BirchBeehivePlanksBlock;
import net.mcreator.feurbuilder.block.AndesiteDeepBlock;
import net.mcreator.feurbuilder.block.AcaciaBeehivePlanksBlock;
import net.mcreator.feurbuilder.FeurBuilderMod;

public class FeurBuilderModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, FeurBuilderMod.MODID);
	public static final RegistryObject<Block> BLACK_LOG = REGISTRY.register("black_log", () -> new BlacklogBlock());
	public static final RegistryObject<Block> BLUE_LOG = REGISTRY.register("blue_log", () -> new BluelogBlock());
	public static final RegistryObject<Block> CYAN_LOG = REGISTRY.register("cyan_log", () -> new CyanlogBlock());
	public static final RegistryObject<Block> GREEN_LOG = REGISTRY.register("green_log", () -> new GreenlogBlock());
	public static final RegistryObject<Block> GREY_LOG = REGISTRY.register("grey_log", () -> new GreyLogBlock());
	public static final RegistryObject<Block> PINK_LOG = REGISTRY.register("pink_log", () -> new PinklogBlock());
	public static final RegistryObject<Block> PURPLE_LOG = REGISTRY.register("purple_log", () -> new PurpleLogBlock());
	public static final RegistryObject<Block> YELLOW_LOG = REGISTRY.register("yellow_log", () -> new YellowLogBlock());
	public static final RegistryObject<Block> BLACK_PLANKS = REGISTRY.register("black_planks", () -> new BlackPlanksBlock());
	public static final RegistryObject<Block> BLUE_PLANKS = REGISTRY.register("blue_planks", () -> new BluePlanksBlock());
	public static final RegistryObject<Block> CYAN_PLANKS = REGISTRY.register("cyan_planks", () -> new CyanPlanksBlock());
	public static final RegistryObject<Block> GREEN_PLANKS = REGISTRY.register("green_planks", () -> new GreenPlanksBlock());
	public static final RegistryObject<Block> GREY_PLANKS = REGISTRY.register("grey_planks", () -> new GreyPlanksBlock());
	public static final RegistryObject<Block> PINK_PLANKS = REGISTRY.register("pink_planks", () -> new PinkPlanksBlock());
	public static final RegistryObject<Block> PURPLE_PLANKS = REGISTRY.register("purple_planks", () -> new PurplePlanksBlock());
	public static final RegistryObject<Block> YELLOW_PLANKS = REGISTRY.register("yellow_planks", () -> new YellowPlanksBlock());
	public static final RegistryObject<Block> BLACK_LEAVES = REGISTRY.register("black_leaves", () -> new BlackLeavesBlock());
	public static final RegistryObject<Block> BLACK_LEAVES_FLOWERING = REGISTRY.register("black_leaves_flowering",
			() -> new BlackLeavesFloweringBlock());
	public static final RegistryObject<Block> BLUE_LEAVES = REGISTRY.register("blue_leaves", () -> new BlueLeavesBlock());
	public static final RegistryObject<Block> BLUELEAVESFLOWERING = REGISTRY.register("blueleavesflowering", () -> new BlueleavesfloweringBlock());
	public static final RegistryObject<Block> CYAN_LEAVES = REGISTRY.register("cyan_leaves", () -> new CyanLeavesBlock());
	public static final RegistryObject<Block> CYAN_LEAVES_FLOWERING = REGISTRY.register("cyan_leaves_flowering",
			() -> new CyanLeavesFloweringBlock());
	public static final RegistryObject<Block> GREEN_LEAVES = REGISTRY.register("green_leaves", () -> new GreenLeavesBlock());
	public static final RegistryObject<Block> GREY_LEAVES = REGISTRY.register("grey_leaves", () -> new GreyLeavesBlock());
	public static final RegistryObject<Block> GREY_LEAVES_FLOWERING = REGISTRY.register("grey_leaves_flowering",
			() -> new GreyLeavesFloweringBlock());
	public static final RegistryObject<Block> PINK_LEAVES = REGISTRY.register("pink_leaves", () -> new PinkLeavesBlock());
	public static final RegistryObject<Block> PINK_LEAVES_FLOWERING = REGISTRY.register("pink_leaves_flowering",
			() -> new PinkLeavesFloweringBlock());
	public static final RegistryObject<Block> PURPLE_LEAVES = REGISTRY.register("purple_leaves", () -> new PurpleLeavesBlock());
	public static final RegistryObject<Block> PURPLE_LEAVES_FLOWERING = REGISTRY.register("purple_leaves_flowering",
			() -> new PurpleLeavesFloweringBlock());
	public static final RegistryObject<Block> YELLOW_LEAVES = REGISTRY.register("yellow_leaves", () -> new YellowLeavesBlock());
	public static final RegistryObject<Block> YELLOW_LEAVES_FLOWERING = REGISTRY.register("yellow_leaves_flowering",
			() -> new YellowLeavesFloweringBlock());
	public static final RegistryObject<Block> BLACK_STAIRS = REGISTRY.register("black_stairs", () -> new BlackStairsBlock());
	public static final RegistryObject<Block> BLACK_SLAB = REGISTRY.register("black_slab", () -> new BlackSlabBlock());
	public static final RegistryObject<Block> BLACK_FENCE = REGISTRY.register("black_fence", () -> new BlackFenceBlock());
	public static final RegistryObject<Block> BLACK_FENCE_GATE = REGISTRY.register("black_fence_gate", () -> new BlackFenceGateBlock());
	public static final RegistryObject<Block> BLUE_STAIRS = REGISTRY.register("blue_stairs", () -> new BlueStairsBlock());
	public static final RegistryObject<Block> BLUE_SLAB = REGISTRY.register("blue_slab", () -> new BlueSlabBlock());
	public static final RegistryObject<Block> BLUE_FENCE = REGISTRY.register("blue_fence", () -> new BlueFenceBlock());
	public static final RegistryObject<Block> BLUE_FENCE_GATE = REGISTRY.register("blue_fence_gate", () -> new BlueFenceGateBlock());
	public static final RegistryObject<Block> CYAN_STAIRS = REGISTRY.register("cyan_stairs", () -> new CyanStairsBlock());
	public static final RegistryObject<Block> CYAN_SLAB = REGISTRY.register("cyan_slab", () -> new CyanSlabBlock());
	public static final RegistryObject<Block> CYAN_FENCE = REGISTRY.register("cyan_fence", () -> new CyanFenceBlock());
	public static final RegistryObject<Block> CYAN_FENCE_GATE = REGISTRY.register("cyan_fence_gate", () -> new CyanFenceGateBlock());
	public static final RegistryObject<Block> GREEN_STAIRS = REGISTRY.register("green_stairs", () -> new GreenStairsBlock());
	public static final RegistryObject<Block> GREEN_SLAB = REGISTRY.register("green_slab", () -> new GreenSlabBlock());
	public static final RegistryObject<Block> GREEN_FENCE = REGISTRY.register("green_fence", () -> new GreenFenceBlock());
	public static final RegistryObject<Block> GREEN_FENCE_GATE = REGISTRY.register("green_fence_gate", () -> new GreenFenceGateBlock());
	public static final RegistryObject<Block> GREY_STAIRS = REGISTRY.register("grey_stairs", () -> new GreyStairsBlock());
	public static final RegistryObject<Block> GREY_SLAB = REGISTRY.register("grey_slab", () -> new GreySlabBlock());
	public static final RegistryObject<Block> GREY_FENCE = REGISTRY.register("grey_fence", () -> new GreyFenceBlock());
	public static final RegistryObject<Block> GREY_FENCE_GATE = REGISTRY.register("grey_fence_gate", () -> new GreyFenceGateBlock());
	public static final RegistryObject<Block> PINK_STAIRS = REGISTRY.register("pink_stairs", () -> new PinkStairsBlock());
	public static final RegistryObject<Block> PINK_SLAB = REGISTRY.register("pink_slab", () -> new PinkSlabBlock());
	public static final RegistryObject<Block> PINK_FENCE = REGISTRY.register("pink_fence", () -> new PinkFenceBlock());
	public static final RegistryObject<Block> PINK_FENCE_GATE = REGISTRY.register("pink_fence_gate", () -> new PinkFenceGateBlock());
	public static final RegistryObject<Block> PURPLE_STAIRS = REGISTRY.register("purple_stairs", () -> new PurpleStairsBlock());
	public static final RegistryObject<Block> PURPLE_SLAB = REGISTRY.register("purple_slab", () -> new PurpleSlabBlock());
	public static final RegistryObject<Block> PURPLE_FENCE = REGISTRY.register("purple_fence", () -> new PurpleFenceBlock());
	public static final RegistryObject<Block> PURPLE_FENCE_GATE = REGISTRY.register("purple_fence_gate", () -> new PurpleFenceGateBlock());
	public static final RegistryObject<Block> YELLOW_STAIRS = REGISTRY.register("yellow_stairs", () -> new YellowStairsBlock());
	public static final RegistryObject<Block> YELLOW_SLAB = REGISTRY.register("yellow_slab", () -> new YellowSlabBlock());
	public static final RegistryObject<Block> YELLOW_FENCE = REGISTRY.register("yellow_fence", () -> new YellowFenceBlock());
	public static final RegistryObject<Block> YELLOW_FENCE_GATE = REGISTRY.register("yellow_fence_gate", () -> new YellowFenceGateBlock());
	public static final RegistryObject<Block> BLACK_LEAVES_WALL = REGISTRY.register("black_leaves_wall", () -> new BlackLeavesWallBlock());
	public static final RegistryObject<Block> BLACK_LEAVES_FLOWERING_WALL = REGISTRY.register("black_leaves_flowering_wall",
			() -> new BlackLeavesFloweringWallBlock());
	public static final RegistryObject<Block> BLUE_LEAVES_WALL = REGISTRY.register("blue_leaves_wall", () -> new BlueLeavesWallBlock());
	public static final RegistryObject<Block> BLUE_LEAVES_FLOWERING_WALL = REGISTRY.register("blue_leaves_flowering_wall",
			() -> new BlueLeavesFloweringWallBlock());
	public static final RegistryObject<Block> CYAN_LEAVES_WALL = REGISTRY.register("cyan_leaves_wall", () -> new CyanLeavesWallBlock());
	public static final RegistryObject<Block> CYAN_LEAVES_FLOWERING_WALL = REGISTRY.register("cyan_leaves_flowering_wall",
			() -> new CyanLeavesFloweringWallBlock());
	public static final RegistryObject<Block> GREEN_LEAVES_WALL = REGISTRY.register("green_leaves_wall", () -> new GreenLeavesWallBlock());
	public static final RegistryObject<Block> GREY_LEAVES_WALL = REGISTRY.register("grey_leaves_wall", () -> new GreyLeavesWallBlock());
	public static final RegistryObject<Block> GREY_LEAVES_FLOWERING_WALL = REGISTRY.register("grey_leaves_flowering_wall",
			() -> new GreyLeavesFloweringWallBlock());
	public static final RegistryObject<Block> PINK_LEAVES_WALL = REGISTRY.register("pink_leaves_wall", () -> new PinkLeavesWallBlock());
	public static final RegistryObject<Block> PINK_LEAVES_FLOWERING_WALL = REGISTRY.register("pink_leaves_flowering_wall",
			() -> new PinkLeavesFloweringWallBlock());
	public static final RegistryObject<Block> PURPLE_LEAVES_WALL = REGISTRY.register("purple_leaves_wall", () -> new PurpleLeavesWallBlock());
	public static final RegistryObject<Block> PURPLE_LEAVES_FLOWERING_WALL = REGISTRY.register("purple_leaves_flowering_wall",
			() -> new PurpleLeavesFloweringWallBlock());
	public static final RegistryObject<Block> YELLOW_LEAVES_WALL = REGISTRY.register("yellow_leaves_wall", () -> new YellowLeavesWallBlock());
	public static final RegistryObject<Block> YELLOW_LEAVES_FLOWERING_WALL = REGISTRY.register("yellow_leaves_flowering_wall",
			() -> new YellowLeavesFloweringWallBlock());
	public static final RegistryObject<Block> OAK_LEAVES_WALL = REGISTRY.register("oak_leaves_wall", () -> new OakLeavesWallBlock());
	public static final RegistryObject<Block> SPRUCE_LEAVES_WALL = REGISTRY.register("spruce_leaves_wall", () -> new SpruceLeavesWallBlock());
	public static final RegistryObject<Block> BIRCH_BEEHIVE_PLANKS = REGISTRY.register("birch_beehive_planks", () -> new BirchBeehivePlanksBlock());
	public static final RegistryObject<Block> ACACIA_BEEHIVE_PLANKS = REGISTRY.register("acacia_beehive_planks",
			() -> new AcaciaBeehivePlanksBlock());
	public static final RegistryObject<Block> SPRUCE_BEEHIVE_PLANKS = REGISTRY.register("spruce_beehive_planks",
			() -> new SpruceBeehivePlanksBlock());
	public static final RegistryObject<Block> BLACK_BEEHIVE_PLANKS = REGISTRY.register("black_beehive_planks", () -> new BlackBeehivePlanksBlock());
	public static final RegistryObject<Block> BLUE_BEEHIVE_PLANKS = REGISTRY.register("blue_beehive_planks", () -> new BlueBeehivePlanksBlock());
	public static final RegistryObject<Block> CRIMSON_BEEHIVE_PLANKS = REGISTRY.register("crimson_beehive_planks",
			() -> new CrimsonBeehivePlanksBlock());
	public static final RegistryObject<Block> CYAN_BEEHIVE_PLANKS = REGISTRY.register("cyan_beehive_planks", () -> new CyanBeehivePlanksBlock());
	public static final RegistryObject<Block> DARK_OAK_BEEHIVE_PLANKS = REGISTRY.register("dark_oak_beehive_planks",
			() -> new DarkOakBeehivePlanksBlock());
	public static final RegistryObject<Block> OAK_BEEHIVE_PLANKS = REGISTRY.register("oak_beehive_planks", () -> new OakBeehivePlanksBlock());
	public static final RegistryObject<Block> GREEN_BEEHIVE_PLANKS = REGISTRY.register("green_beehive_planks", () -> new GreenBeehivePlanksBlock());
	public static final RegistryObject<Block> GREY_BEEHIVE_PLANKS = REGISTRY.register("grey_beehive_planks", () -> new GreyBeehivePlanksBlock());
	public static final RegistryObject<Block> JUNGLE_BEEHIVE_PLANKS = REGISTRY.register("jungle_beehive_planks",
			() -> new JungleBeehivePlanksBlock());
	public static final RegistryObject<Block> MANGROVE_BEEHIVE_PLANKS = REGISTRY.register("mangrove_beehive_planks",
			() -> new MangroveBeehivePlanksBlock());
	public static final RegistryObject<Block> PINK_BEEHIVE_PLANKS = REGISTRY.register("pink_beehive_planks", () -> new PinkBeehivePlanksBlock());
	public static final RegistryObject<Block> PURPLE_BEEHIVE_PLANKS = REGISTRY.register("purple_beehive_planks",
			() -> new PurpleBeehivePlanksBlock());
	public static final RegistryObject<Block> WARPED_BEEHIVE_PLANKS = REGISTRY.register("warped_beehive_planks",
			() -> new WarpedBeehivePlanksBlock());
	public static final RegistryObject<Block> YELLOW_BEEHIVE_PLANKS = REGISTRY.register("yellow_beehive_planks",
			() -> new YellowBeehivePlanksBlock());
	public static final RegistryObject<Block> BLUE_SEALANTERN = REGISTRY.register("blue_sealantern", () -> new BlueSealanternBlock());
	public static final RegistryObject<Block> GREEN_SEALANTERN = REGISTRY.register("green_sealantern", () -> new GreenSealanternBlock());
	public static final RegistryObject<Block> GREY_SEALANTERN = REGISTRY.register("grey_sealantern", () -> new GreySealanternBlock());
	public static final RegistryObject<Block> ORANGE_SEALANTERN = REGISTRY.register("orange_sealantern", () -> new OrangeSealanternBlock());
	public static final RegistryObject<Block> PINK_SEALANTERN = REGISTRY.register("pink_sealantern", () -> new PinkSealanternBlock());
	public static final RegistryObject<Block> PURPLE_SEALANTERN = REGISTRY.register("purple_sealantern", () -> new PurpleSealanternBlock());
	public static final RegistryObject<Block> YELLOW_SEALANTERN = REGISTRY.register("yellow_sealantern", () -> new YellowSealanternBlock());
	public static final RegistryObject<Block> CALCITE_DEEP = REGISTRY.register("calcite_deep", () -> new CalciteDeepBlock());
	public static final RegistryObject<Block> ANDESITE_DEEP = REGISTRY.register("andesite_deep", () -> new AndesiteDeepBlock());
	public static final RegistryObject<Block> CALCITE_SMOOTH = REGISTRY.register("calcite_smooth", () -> new CalciteSmoothBlock());
	public static final RegistryObject<Block> BLACK_BRICKS = REGISTRY.register("black_bricks", () -> new BlackBricksBlock());
	public static final RegistryObject<Block> BLUE_BRICKS = REGISTRY.register("blue_bricks", () -> new BlueBricksBlock());
	public static final RegistryObject<Block> BLUE_BRICKS_CRACKED = REGISTRY.register("blue_bricks_cracked", () -> new BlueBricksCrackedBlock());
	public static final RegistryObject<Block> CYAN_BRICKS = REGISTRY.register("cyan_bricks", () -> new CyanBricksBlock());
	public static final RegistryObject<Block> CYAN_BRICKS_CRACKED = REGISTRY.register("cyan_bricks_cracked", () -> new CyanBricksCrackedBlock());
	public static final RegistryObject<Block> GREEN_BRICKS = REGISTRY.register("green_bricks", () -> new GreenBricksBlock());
	public static final RegistryObject<Block> GREEN_BRICKS_CRACKED = REGISTRY.register("green_bricks_cracked", () -> new GreenBricksCrackedBlock());
	public static final RegistryObject<Block> GREY_BRICKS = REGISTRY.register("grey_bricks", () -> new GreyBricksBlock());
	public static final RegistryObject<Block> GREY_BRICKS_CRACKED = REGISTRY.register("grey_bricks_cracked", () -> new GreyBricksCrackedBlock());
	public static final RegistryObject<Block> ORANGE_BRICKS = REGISTRY.register("orange_bricks", () -> new OrangeBricksBlock());
	public static final RegistryObject<Block> ORANGE_BRICKS_CRACKED = REGISTRY.register("orange_bricks_cracked",
			() -> new OrangeBricksCrackedBlock());
	public static final RegistryObject<Block> PINK_BRICKS = REGISTRY.register("pink_bricks", () -> new PinkBricksBlock());
	public static final RegistryObject<Block> PINK_BRICKS_CRACKED = REGISTRY.register("pink_bricks_cracked", () -> new PinkBricksCrackedBlock());
	public static final RegistryObject<Block> PURPLE_BRICKS = REGISTRY.register("purple_bricks", () -> new PurpleBricksBlock());
	public static final RegistryObject<Block> PURPLE_BRICKS_CRACKED = REGISTRY.register("purple_bricks_cracked",
			() -> new PurpleBricksCrackedBlock());
	public static final RegistryObject<Block> YELLOW_BRICKS = REGISTRY.register("yellow_bricks", () -> new YellowBricksBlock());
	public static final RegistryObject<Block> YELLOW_BRICKS_CRACKED = REGISTRY.register("yellow_bricks_cracked",
			() -> new YellowBricksCrackedBlock());
	public static final RegistryObject<Block> MANGROVE_GLASS = REGISTRY.register("mangrove_glass", () -> new MangroveGlassBlock());
	public static final RegistryObject<Block> MANGROVE_GLASS_PANEL = REGISTRY.register("mangrove_glass_panel", () -> new MangroveGlassPanelBlock());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void blockColorLoad(RegisterColorHandlersEvent.Block event) {
			OakLeavesWallBlock.blockColorLoad(event);
		}
	}
}
