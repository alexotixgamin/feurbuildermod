
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.feurbuilder.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.feurbuilder.FeurBuilderMod;

public class FeurBuilderModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, FeurBuilderMod.MODID);
	public static final RegistryObject<Item> BLACK_LOG = block(FeurBuilderModBlocks.BLACK_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_LOG = block(FeurBuilderModBlocks.BLUE_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_LOG = block(FeurBuilderModBlocks.CYAN_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_LOG = block(FeurBuilderModBlocks.GREEN_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_LOG = block(FeurBuilderModBlocks.GREY_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_LOG = block(FeurBuilderModBlocks.PINK_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_LOG = block(FeurBuilderModBlocks.PURPLE_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_LOG = block(FeurBuilderModBlocks.YELLOW_LOG, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_PLANKS = block(FeurBuilderModBlocks.BLACK_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_PLANKS = block(FeurBuilderModBlocks.BLUE_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_PLANKS = block(FeurBuilderModBlocks.CYAN_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_PLANKS = block(FeurBuilderModBlocks.GREEN_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_PLANKS = block(FeurBuilderModBlocks.GREY_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_PLANKS = block(FeurBuilderModBlocks.PINK_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_PLANKS = block(FeurBuilderModBlocks.PURPLE_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_PLANKS = block(FeurBuilderModBlocks.YELLOW_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_LEAVES = block(FeurBuilderModBlocks.BLACK_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_LEAVES_FLOWERING = block(FeurBuilderModBlocks.BLACK_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_LEAVES = block(FeurBuilderModBlocks.BLUE_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUELEAVESFLOWERING = block(FeurBuilderModBlocks.BLUELEAVESFLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_LEAVES = block(FeurBuilderModBlocks.CYAN_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_LEAVES_FLOWERING = block(FeurBuilderModBlocks.CYAN_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_LEAVES = block(FeurBuilderModBlocks.GREEN_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_LEAVES = block(FeurBuilderModBlocks.GREY_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_LEAVES_FLOWERING = block(FeurBuilderModBlocks.GREY_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_LEAVES = block(FeurBuilderModBlocks.PINK_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_LEAVES_FLOWERING = block(FeurBuilderModBlocks.PINK_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_LEAVES = block(FeurBuilderModBlocks.PURPLE_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_LEAVES_FLOWERING = block(FeurBuilderModBlocks.PURPLE_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_LEAVES = block(FeurBuilderModBlocks.YELLOW_LEAVES, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_LEAVES_FLOWERING = block(FeurBuilderModBlocks.YELLOW_LEAVES_FLOWERING,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_STAIRS = block(FeurBuilderModBlocks.BLACK_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_SLAB = block(FeurBuilderModBlocks.BLACK_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_FENCE = block(FeurBuilderModBlocks.BLACK_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_FENCE_GATE = block(FeurBuilderModBlocks.BLACK_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_STAIRS = block(FeurBuilderModBlocks.BLUE_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_SLAB = block(FeurBuilderModBlocks.BLUE_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_FENCE = block(FeurBuilderModBlocks.BLUE_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_FENCE_GATE = block(FeurBuilderModBlocks.BLUE_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_STAIRS = block(FeurBuilderModBlocks.CYAN_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_SLAB = block(FeurBuilderModBlocks.CYAN_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_FENCE = block(FeurBuilderModBlocks.CYAN_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_FENCE_GATE = block(FeurBuilderModBlocks.CYAN_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_STAIRS = block(FeurBuilderModBlocks.GREEN_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_SLAB = block(FeurBuilderModBlocks.GREEN_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_FENCE = block(FeurBuilderModBlocks.GREEN_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_FENCE_GATE = block(FeurBuilderModBlocks.GREEN_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_STAIRS = block(FeurBuilderModBlocks.GREY_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_SLAB = block(FeurBuilderModBlocks.GREY_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_FENCE = block(FeurBuilderModBlocks.GREY_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_FENCE_GATE = block(FeurBuilderModBlocks.GREY_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_STAIRS = block(FeurBuilderModBlocks.PINK_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_SLAB = block(FeurBuilderModBlocks.PINK_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_FENCE = block(FeurBuilderModBlocks.PINK_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_FENCE_GATE = block(FeurBuilderModBlocks.PINK_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_STAIRS = block(FeurBuilderModBlocks.PURPLE_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_SLAB = block(FeurBuilderModBlocks.PURPLE_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_FENCE = block(FeurBuilderModBlocks.PURPLE_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_FENCE_GATE = block(FeurBuilderModBlocks.PURPLE_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_STAIRS = block(FeurBuilderModBlocks.YELLOW_STAIRS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_SLAB = block(FeurBuilderModBlocks.YELLOW_SLAB, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_FENCE = block(FeurBuilderModBlocks.YELLOW_FENCE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_FENCE_GATE = block(FeurBuilderModBlocks.YELLOW_FENCE_GATE, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_LEAVES_WALL = block(FeurBuilderModBlocks.BLACK_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.BLACK_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_LEAVES_WALL = block(FeurBuilderModBlocks.BLUE_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.BLUE_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_LEAVES_WALL = block(FeurBuilderModBlocks.CYAN_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.CYAN_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_LEAVES_WALL = block(FeurBuilderModBlocks.GREEN_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_LEAVES_WALL = block(FeurBuilderModBlocks.GREY_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.GREY_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_LEAVES_WALL = block(FeurBuilderModBlocks.PINK_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.PINK_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_LEAVES_WALL = block(FeurBuilderModBlocks.PURPLE_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.PURPLE_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_LEAVES_WALL = block(FeurBuilderModBlocks.YELLOW_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_LEAVES_FLOWERING_WALL = block(FeurBuilderModBlocks.YELLOW_LEAVES_FLOWERING_WALL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> OAK_LEAVES_WALL = block(FeurBuilderModBlocks.OAK_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> SPRUCE_LEAVES_WALL = block(FeurBuilderModBlocks.SPRUCE_LEAVES_WALL, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BIRCH_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.BIRCH_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> ACACIA_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.ACACIA_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> SPRUCE_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.SPRUCE_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.BLACK_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.BLUE_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CRIMSON_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.CRIMSON_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.CYAN_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> DARK_OAK_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.DARK_OAK_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> OAK_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.OAK_BEEHIVE_PLANKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.GREEN_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.GREY_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> JUNGLE_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.JUNGLE_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> MANGROVE_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.MANGROVE_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.PINK_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.PURPLE_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> WARPED_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.WARPED_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_BEEHIVE_PLANKS = block(FeurBuilderModBlocks.YELLOW_BEEHIVE_PLANKS,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_SEALANTERN = block(FeurBuilderModBlocks.BLUE_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_SEALANTERN = block(FeurBuilderModBlocks.GREEN_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_SEALANTERN = block(FeurBuilderModBlocks.GREY_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> ORANGE_SEALANTERN = block(FeurBuilderModBlocks.ORANGE_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_SEALANTERN = block(FeurBuilderModBlocks.PINK_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_SEALANTERN = block(FeurBuilderModBlocks.PURPLE_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_SEALANTERN = block(FeurBuilderModBlocks.YELLOW_SEALANTERN, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CALCITE_DEEP = block(FeurBuilderModBlocks.CALCITE_DEEP, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> ANDESITE_DEEP = block(FeurBuilderModBlocks.ANDESITE_DEEP, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CALCITE_SMOOTH = block(FeurBuilderModBlocks.CALCITE_SMOOTH, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLACK_BRICKS = block(FeurBuilderModBlocks.BLACK_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_BRICKS = block(FeurBuilderModBlocks.BLUE_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> BLUE_BRICKS_CRACKED = block(FeurBuilderModBlocks.BLUE_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_BRICKS = block(FeurBuilderModBlocks.CYAN_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> CYAN_BRICKS_CRACKED = block(FeurBuilderModBlocks.CYAN_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_BRICKS = block(FeurBuilderModBlocks.GREEN_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREEN_BRICKS_CRACKED = block(FeurBuilderModBlocks.GREEN_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_BRICKS = block(FeurBuilderModBlocks.GREY_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> GREY_BRICKS_CRACKED = block(FeurBuilderModBlocks.GREY_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> ORANGE_BRICKS = block(FeurBuilderModBlocks.ORANGE_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> ORANGE_BRICKS_CRACKED = block(FeurBuilderModBlocks.ORANGE_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_BRICKS = block(FeurBuilderModBlocks.PINK_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PINK_BRICKS_CRACKED = block(FeurBuilderModBlocks.PINK_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_BRICKS = block(FeurBuilderModBlocks.PURPLE_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> PURPLE_BRICKS_CRACKED = block(FeurBuilderModBlocks.PURPLE_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_BRICKS = block(FeurBuilderModBlocks.YELLOW_BRICKS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> YELLOW_BRICKS_CRACKED = block(FeurBuilderModBlocks.YELLOW_BRICKS_CRACKED,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> MANGROVE_GLASS = block(FeurBuilderModBlocks.MANGROVE_GLASS, FeurBuilderModTabs.TAB_FEUR_BUILDER);
	public static final RegistryObject<Item> MANGROVE_GLASS_PANEL = block(FeurBuilderModBlocks.MANGROVE_GLASS_PANEL,
			FeurBuilderModTabs.TAB_FEUR_BUILDER);

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
